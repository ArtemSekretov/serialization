﻿using System;
using System.IO;

namespace BinaryExtensions
{
	public static class BinaryExtension
	{
		public static void Write7BitEncodedUInt(this BinaryWriter writer, uint value)
		{
			uint v = value;
			while (v >= 0x80) {
				writer.Write((byte) (v | 0x80));
				v >>= 7;
			}
			writer.Write((byte)v);
		}
		
		public static uint Read7BitEncodedUInt(this BinaryReader reader)
		{
			uint count = 0;
			int shift = 0;
			byte b;
			do {
				if (shift == 5 * 7)  // 5 bytes max per Int32, shift += 7
					throw new Exception("Bad7BitInt32 Format");
 
				b = reader.ReadByte();
				count |= (uint)(b & 0x7F) << shift;
				shift += 7;
			} while ((b & 0x80) != 0);
			return count;
		}
		
		public static void Write7BitEncodedULong(this BinaryWriter writer, ulong value)
		{
			ulong v = value;
			while (v >= 0x80) {
				writer.Write((byte) (v | 0x80));
				v >>= 7;
			}
			writer.Write((byte)v);
		}
		
		public static ulong Read7BitEncodedULong(this BinaryReader reader)
		{
			ulong count = 0;
			int shift = 0;
			byte b;
			do {
				if (shift == 10 * 7)  // 5 bytes max per Int32, shift += 7
					throw new Exception("Bad7BitInt32 Format");
 
				b = reader.ReadByte();
				count |= (ulong)(b & 0x7F) << shift;
				shift += 7;
			} while ((b & 0x80) != 0);
			return count;
		}
		
	}
}