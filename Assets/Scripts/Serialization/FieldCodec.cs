﻿using System;
using System.Collections.Generic;
using BinaryExtensions;

namespace Serialization
{
	public static class FieldCodec
	{
		public static FieldCodec<bool> ForBool(uint tag)
		{
			return new FieldCodec<bool>(input => input.Reader.ReadBoolean(), 
				(output, value) => output.Writer.Write(value), 
				(ser, b) => ComputeSize.Bool(b), tag);
		}
		
		public static FieldCodec<int> ForInt32(uint tag)
		{
			return new FieldCodec<int>(input =>
				{
					uint read7BitEncodedUInt = input.Reader.Read7BitEncodedUInt();
					return SerializationHelper.DecodeZigZag32(read7BitEncodedUInt);
				}, 
				(output, value) =>
				{
					uint encodeZigZag32 = SerializationHelper.EncodeZigZag32(value);
					output.Writer.Write7BitEncodedUInt(encodeZigZag32);
				},
				(ser, i) => ComputeSize.Int32(i), tag);
		}
		
		public static FieldCodec<uint> ForUInt32(uint tag)
		{
			return new FieldCodec<uint>(input => input.Reader.Read7BitEncodedUInt(), 
				(output, value) => output.Writer.Write7BitEncodedUInt(value), 
				(ser, u) => ComputeSize.UInt32(u), tag);
		}
		
		public static FieldCodec<long> ForInt64(uint tag)
		{
			return new FieldCodec<long>(input =>
				{
					ulong read7BitEncodedULong = input.Reader.Read7BitEncodedULong();
					return SerializationHelper.DecodeZigZag64(read7BitEncodedULong);
				}, 
				(output, value) =>
				{
					ulong encodeZigZag64 = SerializationHelper.EncodeZigZag64(value);
					output.Writer.Write7BitEncodedULong(encodeZigZag64);
				},
				(ser, l) => ComputeSize.Int64(l), tag);
		}
		
		public static FieldCodec<ulong> ForUInt64(uint tag)
		{
			return new FieldCodec<ulong>(input => input.Reader.Read7BitEncodedULong(), 
				(output, value) => output.Writer.Write7BitEncodedULong(value),
				(ser, u) => ComputeSize.UInt64(u), tag);
		}
		
		public static FieldCodec<string> ForString(uint tag)
		{
			return new FieldCodec<string>(input => input.Reader.ReadString(), 
				(output, value) => output.Writer.Write(value),
				(ser, s) => ComputeSize.String(s), tag);
		}
		
		public static FieldCodec<float> ForFloat(uint tag)
		{
			return new FieldCodec<float>(input => input.Reader.ReadSingle(), 
				(output, value) => output.Writer.Write(value),
				(ser, f) => ComputeSize.Float(f), tag);
		}
		
		public static FieldCodec<short> ForInt16(uint tag)
		{
			return new FieldCodec<short>(input => input.Reader.ReadInt16(), 
				(output, value) => output.Writer.Write(value),
				(ser, s) => ComputeSize.Int16(s), tag);
		}
		
		public static FieldCodec<ushort> ForUInt16(uint tag)
		{
			return new FieldCodec<ushort>(input => input.Reader.ReadUInt16(), 
				(output, value) => output.Writer.Write(value),
				(ser, u) => ComputeSize.UInt16(u), tag);
		}
		
		public static FieldCodec<byte> ForByte(uint tag)
		{
			return new FieldCodec<byte>(input => input.Reader.ReadByte(), 
				(output, value) => output.Writer.Write(value),
				(ser, b) => ComputeSize.Byte(b), tag);
		}

		public static FieldCodec<T> ForSerializationContext<T>(uint tag, SerializationCreator<T> creator)
			where T : ISerializationContext, new()
		{
			return new FieldCodec<T>(input =>
				{
					T result = creator.Create();

					input.PushRoot(result);
					result.Load(input);
					input.PopRoot();
					
					return result;
				},
				(output, value) => value.Save(output), 
				(serialization, value) => value.Size(serialization), tag);
		}
	}
	
	public class FieldCodec<T>
	{		
		public uint Key { get; private set; }

		private Action<TSerialization, T> ValueWriter { get; set; }

		private Func<TSerialization, T, uint> ValueSizeCalculator { get; set; }

		private Func<TSerialization, T> ValueReader { get; set; }

		private int recursionLimit = 32;
		
		private int recursionDepth = 0;
		
		public FieldCodec(Func<TSerialization, T> reader,
			Action<TSerialization, T> writer,
			Func<TSerialization, T, uint> sizeCalculator, uint key)
		{
			ValueWriter = writer;
			ValueSizeCalculator = sizeCalculator;
			ValueReader = reader;
			Key = key;
		}
		
		public void Save(TSerialization serialization, T value, uint fieldNumber)
		{
			recursionDepth++;
			if (recursionDepth >= recursionLimit)
			{
				throw new InvalidOperationException("RecursionLimitExceeded");
			}
			
			if (!IsDefault(value))
			{
				serialization.Writer.Write7BitEncodedUInt(Tag.Make(Key, fieldNumber));
				ValueWriter(serialization, value);
			}

			recursionDepth--;
		}

		public T Load(TSerialization serialization)
		{
			recursionDepth++;
			if (recursionDepth >= recursionLimit)
			{
				throw new InvalidOperationException("RecursionLimitExceeded");
			}
			
			T result = ValueReader(serialization);
		
			recursionDepth--;

			return result;
		}

		public uint Size(TSerialization serialization, T value, uint fieldNumber)
		{
			recursionDepth++;
			if (recursionDepth >= recursionLimit)
			{
				throw new InvalidOperationException("RecursionLimitExceeded");
			}
			
			uint result = 0;
			if (!IsDefault(value))
			{
				uint valueSize = ValueSizeCalculator(serialization, value);
				result = valueSize + ComputeSize.UInt32(Tag.Make(Key, fieldNumber));
			}

			recursionDepth--;
			
			return result;
		}
		
		private bool IsDefault(T value)
		{
			return EqualityComparer<T>.Default.Equals(value, default(T));
		}
	}
}