﻿using System.IO;
using Model;

namespace Serialization
{
	public class TSerializationExtention : TSerialization
	{
		static TSerializationExtention()
		{
			AddCodec(FieldCodec.ForSerializationContext(11, new SerializationCreator<Unit>()));
			AddCodec(FieldCodec.ForSerializationContext(12, new SerializationCreator<TupleUnits>()));
		}
					
		public TSerializationExtention(Stream reader, Stream writer) : base(reader, writer)
		{
		}
		
		public static byte[] Save<T>(T value)
		{
			MemoryStream ms = new MemoryStream();
		
			TSerializationExtention serialization = new TSerializationExtention(null, ms);
		
			serialization.Write(value, 0);

			return ms.ToArray();
		}
		
		public static T Load<T>(byte[] bytes)
		{
			MemoryStream ms = new MemoryStream(bytes);
		
			TSerializationExtention serialization = new TSerializationExtention(ms, null);
		
			return serialization.Read<T>(0);
		}
		
		public static uint Size<T>(T value)
		{
			TSerializationExtention serialization = new TSerializationExtention(null, null);
		
			return serialization.Size(value, 0);
		}
	}
}