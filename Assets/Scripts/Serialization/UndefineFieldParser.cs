﻿namespace Serialization
{

	public class UndefineFieldParser<T> : UndefineFieldParser
	{
		private FieldCodec<T> _codec;
		
		public UndefineFieldParser(FieldCodec<T> codec)
		{
			_codec = codec;
		}

		public override void Parse(uint fieldNumber, UndefineFieldManager fieldManager, TSerialization serialization)
		{
			T value = _codec.Load(serialization);
			
			UndefineFieldManager.WrappedField<T> wrappedField = new UndefineFieldManager.WrappedField<T>(value);
			
			fieldManager.AddField(fieldNumber, wrappedField);
		}
	}
	
	public abstract class UndefineFieldParser
	{
		public abstract void Parse(uint fieldNumber, UndefineFieldManager fieldManager, TSerialization serialization);
	}
}