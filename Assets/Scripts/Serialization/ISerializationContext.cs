﻿namespace Serialization
{
	public interface ISerializationContext
	{
		void Save(TSerialization serialization);
		void Load(TSerialization serialization);
		uint Size(TSerialization serialization);
	}
}