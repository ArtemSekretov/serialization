﻿namespace Serialization
{
	public class SerializationCreator<T> where T : ISerializationContext, new()
	{
		public T Create()
		{
			T result = new T();
			return result;
		}
	}
}