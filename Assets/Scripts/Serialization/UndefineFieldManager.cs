﻿using System;
using System.Collections.Generic;

namespace Serialization
{
	public class UndefineFieldManager
	{
		public class WrappedField<T>
		{
			public T Value { get; private set; }

			public WrappedField(T value)
			{
				Value = value;
			}
		}
		
		private Dictionary<Type, Dictionary<uint, object>> fieldContainer;

		public UndefineFieldManager()
		{
			fieldContainer = new Dictionary<Type, Dictionary<uint, object>>();
		}
		
		public bool TryGetField<T>(uint fieldNumber, out T value)
		{
			bool result = false;
			value = default(T);
			Type type = typeof(T);

			Dictionary<uint, object> fieldNumberToObject;
			if (fieldContainer.TryGetValue(type, out fieldNumberToObject))
			{
				object obj;
				if (fieldNumberToObject.TryGetValue(fieldNumber, out obj))
				{
					WrappedField<T> wrappedField = (WrappedField<T>) obj;
					value = wrappedField.Value;
					result = true;
				}
			}
			
			return result;
		}

		public void AddField<T>(uint fieldNumber, WrappedField<T> wrappedField)
		{
			Type type = typeof(T);
			if (!fieldContainer.ContainsKey(type))
			{
				Dictionary<uint, object> dictionary = new Dictionary<uint, object>();
				dictionary.Add(fieldNumber, wrappedField);
				fieldContainer.Add(type, dictionary);
			}
			else
			{
				if (!fieldContainer[type].ContainsKey(fieldNumber))
				{
					fieldContainer[type].Add(fieldNumber, wrappedField);
				}
			}
		}
		
		public void ParseField(UndefineFieldParser parser, uint fieldNumber, TSerialization serialization)
		{
			parser.Parse(fieldNumber, this, serialization);
		}
	}
}