﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace Serialization
{
	public class TSerialization
	{				
		internal BinaryReader Reader { get; private set; }

		internal BinaryWriter Writer { get; private set; }

		private static Dictionary<Type, object> _codecs = new Dictionary<Type, object>();
		
		private static Dictionary<uint, UndefineFieldParser> _parsers = new Dictionary<uint, UndefineFieldParser>();
		
		private UndefineFieldManager _undefineFieldManager;

		private Dictionary<ISerializationContext, UndefineFieldManager> _undefineFieldManagers;

		private Stack<ISerializationContext> _treeOfSerialization; 
		
		protected static void AddCodec<T>(FieldCodec<T> fieldCodec)
		{
			Type type = typeof(T);
			if (!_codecs.ContainsKey(type))
			{
				_codecs.Add(type, fieldCodec);

				uint fieldCodecKey = fieldCodec.Key;
				_parsers.Add(fieldCodecKey, new UndefineFieldParser<T>(fieldCodec));
			}
			else
			{
				throw new InvalidOperationException("Type already declared: " + typeof(T));
			}
		}
		
		private FieldCodec<T> GetCodec<T>()
		{
			object value;
			if (!_codecs.TryGetValue(typeof(T), out value))
			{
				throw new InvalidOperationException("Invalid type argument requested for wrapper codec: " + typeof(T));
			}
			return (FieldCodec<T>) value;
		}
		
		static TSerialization()
		{
			AddCodec(FieldCodec.ForBool(1));
			AddCodec(FieldCodec.ForByte(2));
			AddCodec(FieldCodec.ForInt16(3));
			AddCodec(FieldCodec.ForUInt16(4));
			AddCodec(FieldCodec.ForInt32(5));
			AddCodec(FieldCodec.ForUInt32(6));
			AddCodec(FieldCodec.ForInt64(7));
			AddCodec(FieldCodec.ForUInt64(8));
			AddCodec(FieldCodec.ForFloat(9));
			AddCodec(FieldCodec.ForString(10));			
		}
	
		public TSerialization(Stream reader, Stream writer)
		{
			_undefineFieldManager = new UndefineFieldManager();
			_undefineFieldManagers = new Dictionary<ISerializationContext, UndefineFieldManager>();
			
			_treeOfSerialization = new Stack<ISerializationContext>();
			_treeOfSerialization.Push(null);
			
			if(reader != null)
			{
				Reader = new BinaryReader(reader, Encoding.Default);
			}
			if(writer != null)
			{
				Writer = new BinaryWriter(writer, Encoding.Default);
			}
		}

		public void Write<T>(T value, uint fieldNumber)
		{
			FieldCodec<T> fieldCodec = GetCodec<T>();
			WrappedFieldCodec.Write(this, value, fieldNumber, fieldCodec);
		}
		
		public T Read<T>(uint fieldNumber)
		{
			FieldCodec<T> fieldCodec = GetCodec<T>();
			return WrappedFieldCodec.Read(this, fieldNumber, fieldCodec);
		}

		public uint Size<T>(T value, uint fieldNumber)
		{
			FieldCodec<T> fieldCodec = GetCodec<T>();
			return WrappedFieldCodec.CalculateSize(this, value, fieldNumber, fieldCodec);
		}
		
		internal void PushRoot(ISerializationContext parent)
		{
			_treeOfSerialization.Push(parent);
		}

		internal void PopRoot()
		{
			if (_treeOfSerialization.Count > 1)
			{
				_treeOfSerialization.Pop();
			}
		}

		internal bool TryAddUnderfineField(uint key, uint fieldNumber)
		{
			bool result = false;

			if (_parsers.ContainsKey(key))
			{
				ISerializationContext parent = _treeOfSerialization.Peek();

				UndefineFieldParser parser = _parsers[key];
				if (parent == null)
				{
					_undefineFieldManager.ParseField(parser, fieldNumber, this);
				}
				else
				{
					if (!_undefineFieldManagers.ContainsKey(parent))
					{
						_undefineFieldManagers.Add(parent, new UndefineFieldManager());
					}
					_undefineFieldManagers[parent].ParseField(parser, fieldNumber, this);
				}

				result = true;
			}

			return result;
		}
		
		internal bool TryGetUnderfineField<T>(uint fieldNumber, out T value)
		{
			ISerializationContext parent = _treeOfSerialization.Peek();			
			return TryGetUnderfineField<T>(parent, fieldNumber, out value);
		}
		
		internal bool TryGetUnderfineField<T>(ISerializationContext parent, uint fieldNumber, out T value)
		{
			bool result = false;
			
			value = default(T);

			if (parent == null)
			{
				result = _undefineFieldManager.TryGetField(fieldNumber, out value);
			}
			else
			{
				UndefineFieldManager fieldManager;
				if (_undefineFieldManagers.TryGetValue(parent, out fieldManager))
				{
					result = fieldManager.TryGetField(fieldNumber, out value);
				}
			}

			return result;
		}
	}
}