﻿using System.Text;

namespace Serialization
{
    public class ComputeSize
    {
        private const uint LittleEndian64Size = 8;
        private const uint LittleEndian32Size = 4;        
        private const uint LittleEndian16Size = 2;        

        public static uint Double(double value)
        {
            return LittleEndian64Size;
        }

        public static uint Float(float value)
        {
            return LittleEndian32Size;
        }

        public static uint UInt64(ulong value)
        {
            return ComputeRawVarint64Size(value);
        }

        public static uint Int64(long value)
        {
            ulong encodeZigZag64 = SerializationHelper.EncodeZigZag64(value);
            return ComputeRawVarint64Size(encodeZigZag64);
        }

        public static uint Int32(int value)
        {
            uint encodeZigZag32 = SerializationHelper.EncodeZigZag32(value);
            return ComputeRawVarint32Size(encodeZigZag32);
        }

        public static uint UInt32(uint value)
        {
            return ComputeRawVarint32Size(value);
        }
        
        public static uint Int16(short value)
        {
            return LittleEndian16Size;
        }

        public static uint UInt16(ushort value)
        {
            return LittleEndian16Size;
        }
        
        public static uint Bool(bool value)
        {
            return 1;
        }
        
        public static uint Byte(byte value)
        {
            return 1;
        }
        
        public static uint String(string value)
        {
            int byteArraySize = Encoding.UTF8.GetByteCount(value);
            return ComputeLengthSize(byteArraySize) + (uint)byteArraySize;
        }

        public static uint ComputeLengthSize(int length)
        {
            return ComputeRawVarint32Size((uint) length);
        }

        protected static uint ComputeRawVarint32Size(uint value)
        {
            if ((value & (0xffffffff << 7)) == 0)
            {
                return 1;
            }
            if ((value & (0xffffffff << 14)) == 0)
            {
                return 2;
            }
            if ((value & (0xffffffff << 21)) == 0)
            {
                return 3;
            }
            if ((value & (0xffffffff << 28)) == 0)
            {
                return 4;
            }
            return 5;
        }

        protected static uint ComputeRawVarint64Size(ulong value)
        {
            if ((value & (0xffffffffffffffffL << 7)) == 0)
            {
                return 1;
            }
            if ((value & (0xffffffffffffffffL << 14)) == 0)
            {
                return 2;
            }
            if ((value & (0xffffffffffffffffL << 21)) == 0)
            {
                return 3;
            }
            if ((value & (0xffffffffffffffffL << 28)) == 0)
            {
                return 4;
            }
            if ((value & (0xffffffffffffffffL << 35)) == 0)
            {
                return 5;
            }
            if ((value & (0xffffffffffffffffL << 42)) == 0)
            {
                return 6;
            }
            if ((value & (0xffffffffffffffffL << 49)) == 0)
            {
                return 7;
            }
            if ((value & (0xffffffffffffffffL << 56)) == 0)
            {
                return 8;
            }
            if ((value & (0xffffffffffffffffL << 63)) == 0)
            {
                return 9;
            }
            return 10;
        }

    }
}