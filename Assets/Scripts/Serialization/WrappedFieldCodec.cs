﻿using System.IO;
using BinaryExtensions;

namespace Serialization
{
	public static class WrappedFieldCodec
	{
		public static T Read<T>(TSerialization serialization, uint fieldNumber, FieldCodec<T> codec)
		{
			T result;
			
			if (!serialization.TryGetUnderfineField(fieldNumber, out result))
			{
				BinaryReader reader = serialization.Reader;

				uint codecKey = codec.Key;
				do
				{
					long currentPosition = reader.BaseStream.Position;
					long streamLength = reader.BaseStream.Length;
					
					if (currentPosition < streamLength)
					{
						uint length = reader.Read7BitEncodedUInt();

						if (length > 0)
						{
							if (currentPosition + length < streamLength)
							{
								uint tag = reader.Read7BitEncodedUInt();

								uint key = Tag.GetKey(tag);
								uint fieldNum = Tag.GetFieldNumber(tag);

								if (codecKey == key && fieldNum == fieldNumber)
								{
									result = codec.Load(serialization);
									break;
								}

								if (codecKey != key && fieldNum == fieldNumber)
								{
									//todo convert type
									break;
								}

								if (codecKey != key || fieldNum != fieldNumber)
								{
									if (!serialization.TryAddUnderfineField(key, fieldNum))
									{
										uint size = ComputeSize.UInt32(tag);
										reader.ReadBytes((int) (length - size));
									}
								}
							}
							else
							{
								serialization.TryGetUnderfineField(fieldNumber, out result);
								break;
							}
						}
						else
						{
							break;
						}
					}
					else
					{
						break;
					}

				} while (true);
			}

			return result;
		}
		
		public static void Write<T>(TSerialization serialization, T value, uint fieldNumber, FieldCodec<T> codec)
		{
			serialization.Writer.Write7BitEncodedUInt(codec.Size(serialization, value, fieldNumber));
			codec.Save(serialization, value, fieldNumber);
		}

		public static uint CalculateSize<T>(TSerialization serialization, T value, uint fieldNumber, FieldCodec<T> codec)
		{
			uint size = codec.Size(serialization, value, fieldNumber);
			return ComputeSize.UInt32(size) + size;
		}
	}
}