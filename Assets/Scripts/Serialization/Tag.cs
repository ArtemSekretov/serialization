﻿namespace Serialization
{
	public static class Tag
	{
		private const int TagTypeBits = 7;
		private const uint TagTypeMask = (1 << TagTypeBits) - 1;

		public static uint GetFieldNumber(uint tag)
		{
			return tag & TagTypeMask;
		}

		public static uint GetKey(uint tag)
		{
			return tag >> TagTypeBits;
		}

		public static uint Make(uint key, uint fieldNumber)
		{
			return (key << TagTypeBits) | fieldNumber;
		}
	}
}