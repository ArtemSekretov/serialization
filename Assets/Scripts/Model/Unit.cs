﻿using System.Collections.Generic;
using System.Text;
using Serialization;

namespace Model
{
	public class Unit : ISerializationContext
	{
		public int Id { get; set; }

		public string Name { get; set; }

		public bool InGame { get; set; }

		//public List<Unit> Units { get; set; }

		public Unit()
		{
			//Units = new List<Unit>();
		}
		
		public void Save(TSerialization serialization)
		{
			serialization.Write(Id, 0);
			serialization.Write(Name, 1);
			serialization.Write(InGame, 2);
			
			/*serialization.Write(Units.Count, 2);

			foreach (Unit unit in Units)
			{
				serialization.Write(unit, 3);
			}*/
		}

		public void Load(TSerialization serialization)
		{
			Id = serialization.Read<int>(0);
			//InGame = serialization.Read<bool>(2);
			Name = serialization.Read<string>(1);
			
			/*int count = serialization.Read<int>(2);

			for (int i = 0; i < count; i++)
			{
				Units.Add(serialization.Read<Unit>(3));
			}*/
		}

		public uint Size(TSerialization serialization)
		{
			uint size = serialization.Size(Id, 0) +
			            serialization.Size(Name, 1) +
			            serialization.Size(InGame, 2);
			            //serialization.Size(Units.Count, 2);

			/*foreach (Unit unit in Units)
			{
				size += serialization.Size(unit, 3);
			}*/

			return size;
		}

		public override string ToString()
		{
			StringBuilder sb = new StringBuilder();

			sb.AppendLine("id > " + Id + " name > " + Name + " ingame " + InGame);

			/*foreach (Unit unit in Units)
			{
				sb.AppendLine(unit.ToString());
			}*/

			return sb.ToString();
		}
	}
}