﻿using System.Collections;
using System.Collections.Generic;
using Serialization;
using UnityEngine;

namespace Model
{
	public class TupleUnits : ISerializationContext
	{
		public Unit Unit1 { get; set; }
		public Unit Unit2 { get; set; }

		public TupleUnits()
		{
			
		}
		
		public void Save(TSerialization serialization)
		{
			serialization.Write(Unit1, 0);
			serialization.Write(Unit2, 1);
		}

		public void Load(TSerialization serialization)
		{
			Unit1 = serialization.Read<Unit>(0);
			Unit2 = serialization.Read<Unit>(1);
		}

		public uint Size(TSerialization serialization)
		{
			uint size = serialization.Size(Unit1, 0) +
			            serialization.Size(Unit2, 1);

			return size;
		}

		public override string ToString()
		{
			return Unit1 + " " + Unit2;
		}
	}
}