﻿using System.Collections;
using System.Collections.Generic;
using Model;
using Serialization;
using UnityEngine;

public class TestSerialization : MonoBehaviour {

	// Use this for initialization
	void Start () {
		
		Unit unit1 = new Unit();

		unit1.Id = 5;
		unit1.Name = "Test 1";
		unit1.InGame = true;
		
		Unit unit2 = new Unit();

		unit2.Id = 500;
		unit2.Name = "Test 2";
		unit2.InGame = true;
		
		TupleUnits tupleUnits = new TupleUnits();
		tupleUnits.Unit1 = unit1;
		tupleUnits.Unit2 = unit2;

		byte[] bytes = TSerializationExtention.Save(tupleUnits);
		
		Debug.Log("size bytes > " + bytes.Length);
		Debug.Log("size > " + TSerializationExtention.Size(tupleUnits));

		TupleUnits tu = TSerializationExtention.Load<TupleUnits>(bytes);
		
		Debug.Log(tu);

	}
	
	// Update is called once per frame
	void Update () {
		
	}
}
